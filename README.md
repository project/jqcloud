INTRODUCTION
------------
# jQCloud

jQCloud this is a drupal module displays taxonomy terms in blocks.
Integrated with [jQCloud](https://github.com/mistic100/jQCloud) jQuery plugin.

REQUIREMENTS
------------
[jQCloud](https://github.com/mistic100/jQCloud) jQuery plugin.
Install it over "/libraries/jqcloud" folder.

RECOMMENDED MODULES
-------------------
No modules required.

INSTALLATION
------------
## How to install
1. Install the module.
2. Install jQCloud library. [Download](https://github.com/mistic100/jQCloud/
archive/v2.0.3.zip) and extract into "/libraries/jqcloud" folder.
3. Enable the jQCloud module.


CONFIGURATION
-------------
## How to use
The jQCloud module generate a block for each taxonomy vocabulary.

Steps:
1. Go to the Block layout page (/admin/structure/block).
2. Place a block with name "jQCloud with [VOCABULARY_NAME] vocabulary"
in the region you need.

TROUBLESHOOTING
---------------


MAINTAINERS
-----------
  * Ivan Tibezh (tibezh) - https://www.drupal.org/u/tibezh
  * Lilian Catanoi (liliancatanoi90) - https://www.drupal.org/u/liliancatanoi90

This project is sponsored by:
 * [OPTASY](https://www.optasy.com) is a Canadian Drupal development & web
  development company based in Toronto. In the past we provided Drupal
  development solutions for a variety of Canadian and foregin companies with
  outstanding results.
